"use strict";

//Recupérer tout ce qui a été stocker dans le localStorage à chaque actualisation de la page
	ol.innerHTML = localStorage.getItem("list");

	const todos = document.querySelectorAll(".click");
	for(let span of todos){
		span.onclick = () => click(span.parentElement);
	}

	const doings = document.querySelectorAll(".fair");
	for(let span of todos){
		span.onclick = () => fair(span.parentElement);
	}

	const dones = document.querySelectorAll(".color3");
	for(let span of todos){
		span.onclick = () => color3(span.parentElement);
	}

//Une fonction qui nous permet d'ajouter des taches dans la liste
form.onsubmit = () =>{

	//Ajout d'un li dans la liste ordonnée
	const li = document.createElement("li");
	
	//création d'une classe pour la mise en forme de notre liste
	const texte = document.createElement("span");//le span
	texte.classList.add("texte");//ajout de la classe texte
	texte.textContent = champ.value;//Recupère le texte du champ

	//Création du bouton to do
	const todo = document.createElement("span");
	todo.classList.add("click", "action");//ajout de la classe click
	todo.textContent = "to do";//Ajout du texte doing

	//Création du bouton to do
	const doing = document.createElement("span");
	doing.classList.add("fair", "action");//ajout de la classe fair
	doing.textContent = "doing";//Ajout du texte doing

	//Création du bouton to do
	const done = document.createElement("span");
	done.classList.add("color3", "action");//ajout de la classe color3
	done.textContent = "done";//Ajout du texte doing
	
	//ajout de l'event click, fair et color3 sur les span créées
	todo.onclick = () => click(li);

	doing.onclick = () => fair(li);

	done.onclick = () => color3(li);

//Pour rappeler une balise HTML dans notre li
	li.appendChild(texte); 

//Ajouter l'enfant à son parent c.a.d li
	li.appendChild(todo);
	li.appendChild(doing);
	li.appendChild(done);
	

//Ajouter notre li à son parent c.a.d le ol
	ol.appendChild(li); //ajout du li à ol

//Effacer le saisi une fois ajouté
	champ.value = "";

//Création d'un localStorage pour stocker tout le code html creer avec le ol.innerhtml
	localStorage.setItem("list", ol.innerHTML);

//Empécher le formulaire de renvoyer les données par défaut c.a.d l'actualisation automatique
	return false;
}
 
//fonction pour exprimer le click
function click(element){
	element.classList.toggle("click");
	element.classList.toggle("red");
}

//fonction pour changer de couleur lors du click
function fair(element){
	element.classList.toggle("fair");
	element.classList.toggle("orange");
}

//fonction pour changer de couleur lors du click
function color3(element){
	element.classList.toggle("color3");
	element.classList.toggle("green");
}